[![Status](https://img.shields.io/badge/status-active-success.svg)]()
[![Bitbucket Issues](https://img.shields.io/bitbucket/issues/julianvogels/nachbarschaftshilfe-covid-19.svg)](https://bitbucket.org/julianvogels/nachbarschaftshilfe-covid-19/issues)
[![License](https://img.shields.io/badge/license-ISC-blue.svg)](/LICENSE)

---

A web app to generate custom PDF files during the COVID-19 pandemic to put up in your house hallway to inform your neighbors that you are willing to help them out with groceries and other chores that they cannot/shouldn't do.

## 📝 Table of Contents

- About
- Getting Started
- Built With
- TODO
- Contributing
- Authors
- Acknowledgments

## 🧐 About

During the current COVID-19 pandemic some parts of the population are particularly affected and vulnerable to the virus. This web app aims to help these risk groups by empowering others to help. 

The app runs on Node.js/Express and uses [PDFKit](http://pdfkit.org/) to render a PDF based on form input from a user. The generated PDF can then be downloaded as an attachment by sending the form.

## 🏁 Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Tou need [Node.js](https://nodejs.org/) installed on your computer. `npm` comes preinstalled. After cloning the repository, use the Terminal to navigate to the project folder. Then type

```
npm install
```
to install any prerequisites. I recommend using [VSCodium](https://github.com/VSCodium/vscodium) as your text editor when developing this project further.

### Run locally

To start a local webserver, run

```
npm start
```

or use [nodemon](https://github.com/remy/nodemon) to automatically restart the server when there are changes in the code.

```
nodemon
```

Here's what you should be seeing after opening [localhost:3000](http://localhost:3000) in your web browser:

![Website Screenshot](public/images/screenshot.png "Website screenshot")

## 🛠 Built Using

- [PDFKit](http://pdfkit.org/) - PDF creation

## 💪 TODO

- Automated testing
- Better error handling

## ❤️ Contributing

- This repo uses [git flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) in standard configuration. If you want to contribute, please create a `feature` or `bugfix` branch and open a pull request to `develop`. Pushing directly to `develop` or `master` is not permitted.

## ✍️ Authors

- [@julianvogels](https://bitbucket.org/julianvogels/) - Idea & Initial work

## 🎉 Acknowledgements

- [Mozilla's pdf.js base64 example](https://github.com/mozilla/pdf.js/blob/master/examples/learning/helloworld64.html) - Inspired how to best render the PDF
- [Robert Koch Institute](https://www.rki.de) – Trustworthy data
