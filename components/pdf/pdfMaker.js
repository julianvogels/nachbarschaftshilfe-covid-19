const PDFDocument = require("pdfkit");
const getStream = require("get-stream");
const SVGtoPDF = require("svg-to-pdfkit");
const fs = require('fs').promises;

PDFDocument.prototype.addSVG = function(svg, x, y, options) {
  return SVGtoPDF(this, svg, x, y, options), this;
};

const copy_normal = {
  ORGANIZATION: "Nachbarschaftshilfe COVID-19",
  TITLE:        "Hallo Nachbar!",
  BODY:         "Du bist in der COVID-19 Risikogruppe und möchtest nicht für Besorgungen aus dem Haus? Ich gehe gerne für dich einkaufen, zur Post, oder übernehme andere Erledigungen.",
  CONTACT_PREFIX: "Melde dich bei "
};

const copy_formal = {
  ORGANIZATION: "Nachbarschaftshilfe COVID-19",
  TITLE:        "Liebe Nachbarn,",
  BODY:         "Sie sind in der COVID-19 Risikogruppe und möchten nicht für Besorgungen aus dem Haus? Ich gehe gerne für Sie einkaufen, zur Post, oder übernehme andere Erledigungen.",
  CONTACT_PREFIX: "Melden Sie sich bei "
};

const pdfMaker = {
  t: function(key, style) {
    switch (style) {
      case 1: {
        // Normal
        return copy_normal[key];
      }
      case 2: {
        // Formal
        return copy_formal[key];
      }
      default:
        return undefined;
    } 
  },
  readSVG: async function(path) {
      const data = await fs.readFile(path);
      return data.toString();
  },
  make: async function(context) {
    const doc = new PDFDocument({
      size: 'A4',
      layout: 'portrait',
      info: {
        Title: "Aushang Nachbarschaftshilfe COVID-19",
        Author: context.name,
        Keywords: ["COVID-19", "Corona", "Nachbarschaftshilfe"]
      },
      compress: false
    });

    // Style
    const writing_style = context.writing_style;

    // Layout
    const margin_left = 50;

    // Font
    const h1_fontsize = 18
    const body_fontsize = 16
    const list_fontsize = 13
    const source_fontsize = 8
    const footer_note_fontsize = 12

    // SVGs
    const grin_beam = await pdfMaker.readSVG("public/images/font-awesome/regular/grin-beam.svg")
    const hand_point_right = await pdfMaker.readSVG("public/images/font-awesome/regular/hand-point-right.svg")

    // Top right logo
    const logo_size = 30;
    doc.font("public/fonts/open-sans/OpenSans-Semibold.ttf", 14)
    doc.text(pdfMaker.t("ORGANIZATION", writing_style), 345, 5, {
      lineBreak: false
    });
    doc.addSVG(grin_beam, 555, 0, {
      width: logo_size,
      height: logo_size
    });

    // Headline
    doc.font("public/fonts/open-sans/OpenSans-Bold.ttf", h1_fontsize)
    doc.text(pdfMaker.t("TITLE", writing_style), margin_left, 80);
    
    // Body text
    doc.moveDown()
    doc.font("public/fonts/open-sans/OpenSans-Light.ttf", body_fontsize)
    let body_text = pdfMaker.t("BODY", writing_style)
    doc.text(body_text, {
        width: 412,
        align: "left",
        indent: 0,
        columns: 1,
        height: 300,
        ellipsis: true
    });

    const is_empty = function(object) {
      return object == undefined || object == ''
    }

    doc.moveDown()
    let contact_text = ""
    if (!is_empty(context.name)) {
      contact_text += pdfMaker.t("CONTACT_PREFIX", writing_style) + context.name
      if (!is_empty(context.floor)) {
        contact_text += " aus dem " + context.floor
      }
      if (!is_empty(context.phone)) {
        if (!is_empty(context.floor)) {
          contact_text += ". Erreichbar unter " + context.phone
        } else {
          contact_text += " unter " + context.phone
        }
      }
      if (!is_empty(context.floor) || !is_empty(context.phone)) {
        contact_text += "."
      }
    }
    doc.text(contact_text)

    // Risikogruppen Info
    const well_margin_left = margin_left + 20;
    const risikogruppe_y = 320;

    // Well
    doc.roundedRect(margin_left, risikogruppe_y, 500, 230, 10)
    doc.fillColor("#efefefe")
    doc.strokeColor("#ebebeb")
    doc.fillAndStroke()

    doc.fillColor("black")
    doc.strokeColor("black")

    // Headline
    doc.font("public/fonts/open-sans/OpenSans-Semibold.ttf", list_fontsize)
    doc.text("Wer ist in der Risikogruppe?", well_margin_left, risikogruppe_y + 12)
    doc.moveDown()

    // List
    const list_text_indent = 30; 
    const list_icon_size = 20;
    doc.font("public/fonts/open-sans/OpenSans-Light.ttf", list_fontsize)

    const list_text = [
      "Alle ab 50-60 Jahre oder älter",
      "Menschen mit Grunderkrankungen wie z.B. Herzkreislauferkrankungen, Diabetes, Erkrankungen des Atmungssystems (Asthma!), der Leber und der Niere sowie Krebserkrankungen",
      "Menschen mit Immunschwäche (auch bei Einnahme von z.B. Cortison)"
    ]
    for (const list_item of list_text) {
      const y = doc.y
      doc.text(list_item,  well_margin_left + list_text_indent, y)
      doc.addSVG(hand_point_right, doc.x - list_text_indent, y, {
        width: list_icon_size,
        height: list_icon_size
      });
      doc.moveDown()
    }

    doc.moveDown()

    // Source
    doc.fontSize(source_fontsize)
    doc.text("Quelle: https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Risikogruppen.html", 190, doc.y)



    // Calculate longest string
    doc.font('public/fonts/open-sans/OpenSans-Semibold.ttf')
    doc.fontSize(18)
    let name_width = doc.widthOfString(context.name);
    doc.font('public/fonts/open-sans/OpenSans-Regular.ttf')
    doc.fontSize(14)
    let phone_width = doc.widthOfString(context.phone);
    let stockwerk_width = doc.widthOfString(context.floor);

    let max_width = Math.max(phone_width, stockwerk_width, name_width);

    // Website URL
    let website_y = Math.max(Math.min(615, 800 - max_width), 560);
    doc.fontSize(footer_note_fontsize)
    doc.text(process.env.SHARE_URL, margin_left, website_y, {
      width: 500,
      align: "center",
      indent: 0,
      columns: 1,
      ellipsis: true
    });
    
    // Cut off info
    // Horizontal line
    let hr_y_pos = website_y + 25;
    doc.moveTo(0, hr_y_pos)
    doc.lineTo(650, hr_y_pos)
    doc.lineWidth(0.5)
    doc.dash(5, {space: 10})
    doc.stroke()

    // Rotated content
    doc.save();
    doc.rotate(90, { origin: [0, 0] });

    let i;
    let y_pos = hr_y_pos + 15;
    let line_offset = 10;
    let line_length = 290
    for (i = 0; i < 6; i++) {
      let offset = 10 + (i + 1) * -100;
      doc.fill('black');
      doc.font('public/fonts/open-sans/OpenSans-Semibold.ttf')
      doc.fontSize(18)
      doc.text(context.name, y_pos, offset, {lineBreak: false});
      doc.font('public/fonts/open-sans/OpenSans-Regular.ttf')
      doc.fontSize(16)
      doc.text("Nachbarschaftshilfe", y_pos, offset + 25, {lineBreak: false});
      doc.fontSize(14)
      doc.fill('gray');
      doc.text(context.phone, y_pos, offset + 45, {lineBreak: false});
      doc.text(context.floor, y_pos, offset + 60, {lineBreak: false});

      if (i < 5) {
        doc.moveTo(y_pos, offset - line_offset)
        doc.lineTo(y_pos + line_length, offset - line_offset)
        doc.stroke()
      }
    }
    // Restore rotation
    doc.restore();

    return doc;
  },
  base64: async function(context) {
    const doc = await pdfMaker.make(context);
    doc.end();
    const pdfBuffer = await getStream.buffer(doc);
    const base64string = pdfBuffer.toString("base64");

    return base64string;
  }
};

module.exports = pdfMaker;
