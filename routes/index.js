const express = require("express")
const router = express.Router()
const pdfMaker = require("../components/pdf/pdfMaker")

function deparam(query) {
  var pairs, i, keyValuePair, key, value, map = {};
  // remove leading question mark if its there
  if (query.slice(0, 1) === '?') {
      query = query.slice(1);
  }
  if (query !== '') {
      pairs = query.split('&');
      for (i = 0; i < pairs.length; i += 1) {
          keyValuePair = pairs[i].split('=');
          key = decodeURIComponent(keyValuePair[0]);
          value = (keyValuePair.length > 1) ? decodeURIComponent(keyValuePair[1]) : undefined;
          map[key] = value;
      }
  }
  return map;
}

function parse_formdata(formdata) {

  // Transform Schreibstil form data
  let stil = 1
  if (formdata.writing_style == "normal") {
    stil = 1
  } else if (formdata.writing_style == "formal") {
    stil = 2
  }

  return {
    name: formdata.name,
    phone: formdata.phone,
    floor: formdata.floor,
    writing_style: stil,
    filename: "COVID-19-Aushang-"+ formdata.name +".pdf"
  }
}

/* GET home page. */
router.get("/", async (req, res, next) => {
  res.render("index", {})
});

/* AJAX POST update PDF */
router.post('/updatePDF', async (req, res) => {

  // default data
  let formdata = {
    name: "",
    phone: "",
    floor: "",
    writing_style: 1,
    filename: "COVID-19-Aushang.pdf"
  }
  if (req.body.data != undefined) {
    formdata = deparam(req.body.data)
  }
  
  const contextObject = parse_formdata(formdata)
  const pdfBase64string = await pdfMaker.base64(contextObject)
  res.send(pdfBase64string)
});

/* Save PDF */
router.post('/savePDF', async (req, res) => {

  // TODO: Error handling

  const contextObject = parse_formdata(req.body)

  // Setting response to 'attachment' (download).
  // If you use 'inline' here it will automatically open the PDF
  res.setHeader("Content-Disposition", "attachment; filename*=UTF-8''" + encodeURIComponent(contextObject.filename))
  res.setHeader("Content-type", "application/pdf")

  const doc = await pdfMaker.make(contextObject)

  doc.pipe(res)
  doc.end()
});

module.exports = router
