$(document).ready(function() {
  var updateRequest = null;

  const updatePDF = function() {
    $(".ascii-spinner").show();
    if (updateRequest != null) {
      updateRequest.abort();
    }
    const formdata = $("#pdf-form").serialize();
    var parameters = {
      data: formdata
    };
    updateRequest = $.post("/updatePDF", parameters, function(data) {
      renderPDF(data).then(
        function success() {
          // PDF was rendered
          $(".ascii-spinner").hide();
        },
        function error(reason) {
          $(".ascii-spinner").hide();
        }
      );
    }).fail(function(jqXHR, textStatus, errorThrown) {
      // AJAX request was aborted
    });
  };

  $("input.update-on-change").on("input", function() {
    updatePDF();
  });

  $("body").on("change", "input.update-on-change", function() {
    updatePDF();
  });

  // Hide activity spinner
  $(".ascii-spinner").hide();

  // Activity indication spinner when PDF is updating
  var spinner =
    "⡀⡁⡂⡃⡄⡅⡆⡇⡈⡉⡊⡋⡌⡍⡎⡏⡐⡑⡒⡓⡔⡕⡖⡗⡘⡙⡚⡛⡜⡝⡞⡟⡠⡡⡢⡣⡤⡥⡦⡧⡨⡩⡪⡫⡬⡭⡮⡯⡰⡱⡲⡳⡴⡵⡶⡷⡸⡹⡺⡻⡼⡽⡾⡿⢀⢁⢂⢃⢄⢅⢆⢇⢈⢉⢊⢋⢌⢍⢎⢏⢐⢑⢒⢓⢔⢕⢖⢗⢘⢙⢚⢛⢜⢝⢞⢟⢠⢡⢢⢣⢤⢥⢦⢧⢨⢩⢪⢫⢬⢭⢮⢯⢰⢱⢲⢳⢴⢵⢶⢷⢸⢹⢺⢻⢼⢽⢾⢿⣀⣁⣂⣃⣄⣅⣆⣇⣈⣉⣊⣋⣌⣍⣎⣏⣐⣑⣒⣓⣔⣕⣖⣗⣘⣙⣚⣛⣜⣝⣞⣟⣠⣡⣢⣣⣤⣥⣦⣧⣨⣩⣪⣫⣬⣭⣮⣯⣰⣱⣲⣳⣴⣵⣶⣷⣸⣹⣺⣻⣼⣽⣾⣿";
  var i = 0;
  setInterval(function() {
    $(".ascii-spinner").html(spinner[i]);
    i = (i + 1) % spinner.length;
  }, 100);

  // Copy to clipboard
  // Hide button if unsupported
  if (!ClipboardJS.isSupported()) {
    $("#copySharableURL").hide();
    return;
  }

  let clipboard = new ClipboardJS("#copySharableURL");
  clipboard.on("success", function(e) {
    $("#copySharableURL").html(
      "<i class='fas fa-check-circle'></i>&nbsp;Kopiert!"
    );
    e.clearSelection();
  });
  clipboard.on("error", function(e) {
    // Compatibility: Older browsers don't support 'execCommand'
    // so the user has to do that themselves
    $("#copySharableURL").html(
      "<i class='fas fa-check-circle'></i>&nbsp;Jetzt Ctrl+C drücken!"
    );
  });

  updatePDF();
});
